## Indicadores de qualidade

[![Build Status](https://secure.travis-ci.org/gpupo/common.png?branch=master)](http://travis-ci.org/gpupo/common)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/gpupo/common/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/gpupo/common/?branch=master)
[![Code Climate](https://codeclimate.com/github/gpupo/common/badges/gpa.svg)](https://codeclimate.com/github/gpupo/common)
[![Code Coverage](https://scrutinizer-ci.com/g/gpupo/common/badges/coverage.png?b=master)](https://scrutinizer-ci.com/g/gpupo/common/?branch=master)
[![Test Coverage](https://codeclimate.com/github/gpupo/common/badges/coverage.svg)](https://codeclimate.com/github/gpupo/common/coverage)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/c74618a2-45c9-4d12-922a-704b23f7c607/mini.png)](https://insight.sensiolabs.com/projects/c74618a2-45c9-4d12-922a-704b23f7c607)
