### Common\Entity\ArrayCollection


- [x] To array indexed
- [x] To array associative
- [x] To array mixed
- [x] First indexed
- [x] First associative
- [x] First mixed
- [x] Last indexed
- [x] Last associative
- [x] Last mixed
- [x] Key indexed
- [x] Key associative
- [x] Key mixed
- [x] Next indexed
- [x] Next associative
- [x] Next mixed
- [x] Current indexed
- [x] Current associative
- [x] Current mixed
- [x] Get keys indexed
- [x] Get keys associative
- [x] Get keys mixed
- [x] Get values indexed
- [x] Get values associative
- [x] Get values mixed
- [x] Count indexed
- [x] Count associative
- [x] Count mixed
- [x] Remove
- [x] Remove element
- [x] Contains key
- [x] Empty
- [x] Contains
- [x] Exists
- [x] Index of
- [x] Get

### Common\Entity\Collection


- [x] Possui acesso singleton
- [x] Possui métodos getters e setters mágicos
- [x] Métodos getters mágicos possibilitam acesso a propriedades camel case ou snake case
- [x] Possui estrutura de informacao 
- [x] To array indexed
- [x] To array associative
- [x] To array mixed
- [x] First indexed
- [x] First associative
- [x] First mixed
- [x] Last indexed
- [x] Last associative
- [x] Last mixed
- [x] Key indexed
- [x] Key associative
- [x] Key mixed
- [x] Next indexed
- [x] Next associative
- [x] Next mixed
- [x] Current indexed
- [x] Current associative
- [x] Current mixed
- [x] Get keys indexed
- [x] Get keys associative
- [x] Get keys mixed
- [x] Get values indexed
- [x] Get values associative
- [x] Get values mixed
- [x] Count indexed
- [x] Count associative
- [x] Count mixed
- [x] Remove
- [x] Remove element
- [x] Contains key
- [x] Empty
- [x] Contains
- [x] Exists
- [x] Index of
- [x] Get

Tools\Datetime\Holidays
- [x] ``listOfHolidays()`` 
- [x] ``isHoliday()`` 

### Common\Tools\StringTool


- [x] Converte camel case para snake case 

### Common\Traits\GettersTypeTrait


- [x] Get type float
- [x] Get type boolean

### Common\Traits\LoggerTrait


- [x] Implements logger interface

### Common\Traits\MagicCallTrait


- [x] Has magic methods

### Common\Traits\OptionsTrait


- [x] Implements options interface
- [x] Has options container

### Common\Traits\SingletonTrait


- [x] Has singleton instance access

